**Repository location moved! Please check [https://github.com/MaastrichtU-CDS/fair_tools_fair-searchbox](https://github.com/MaastrichtU-CDS/fair_tools_fair-searchbox)**
# FAIR SearchBox

## Installation

### Setup libraries
```
python -m venv .\env
.\env\Scripts\Activate.ps1
pip install -r requirements.txt
```

## Run the application
```
.\env\Scripts\Activate.ps1
python run.py
```